package clase.jsonparse;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ItemAdapter extends ArrayAdapter<CommentBlog> {

    private Context context;
    private ArrayList<CommentBlog> items;
    TextView textView_name, textView_description;

//Declaring items
    public ItemAdapter(Context context, int resource, ArrayList<CommentBlog> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
    }

//Declaring items in the rowView, so they can be seen in the MainActivity
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        View rowView = convertView;
        if (rowView == null) {
            // Create a new view into the list
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item, parent, false);
        }

        // Set data into the view.
        TextView name_category = (TextView) rowView.findViewById(R.id.name_category);
        TextView description = (TextView) rowView.findViewById(R.id.description);


        //findViewByID
        textView_name = (TextView) rowView.findViewById(R.id.textView_name);
        textView_description = (TextView) rowView.findViewById(R.id.textView_description);


        //get Item
        CommentBlog item = this.items.get(position);

        name_category.setText(item.getName_category());
        description.setText(item.getDescription());


        return rowView;



    }

}

