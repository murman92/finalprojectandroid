package clase.jsonparse;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.BoolRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Comment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static android.R.attr.x;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    //declaring
    public ListView list;
    public ArrayList<CommentBlog> commentList = new ArrayList<>();
    public String jsonStr="";
    public ListAdapter adapter;
    private  final Handler handler= new Handler();
    private Timer timer;
    Button button_new;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       //HEADER
        getSupportActionBar().setTitle("GABRIELLAS FINAL PROJECT");

        //FindViewById
        button_new = (Button) findViewById(R.id.button_new);

        //BTN NEW COMMENT
        button_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SecondActivity.class);
                startActivity(i);
                finish();

            }
        });


        //Access and update the comments from the API every 1 minute
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            new GetComments().execute("http://10.0.2.2/final_project/comment/api/comment/getData.php");

                        } catch (Exception e) {
                            Log.d(TAG ,e.getMessage());
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 60000); //execute every 1 minute



    }


    private class GetComments extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //findViewByIds
            list = (ListView) findViewById(R.id.list);

            //CLEAN LIST
            adapter = new ItemAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,commentList);
            commentList.clear();
            list.setAdapter(null);

            //IS CONNECTED
            if(!isConnected()){
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
            }

        }

        //Update the JSON file every 1 minute, and send toast to declare error or success
        @Override
        protected String doInBackground(String... arg) {

            if(isConnected()){
                // Making a request to url and getting response
                HttpHandler sh = new HttpHandler();
                jsonStr = sh.makeServiceCall(arg[0]);
            }else{

                String filename="comment.json";
                StringBuffer stringBuffer = new StringBuffer();

                try {
                    BufferedReader inputReader = new BufferedReader(new InputStreamReader(openFileInput(filename)));
                    String inputString;

                    while ((inputString = inputReader.readLine()) != null) {
                        stringBuffer.append(inputString);
                    }

                    jsonStr = stringBuffer.toString();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (ParseJson())
                return "AsyncTask finished";
            else
                return "Error";
        }

        //If connected, show toast "data has been updated"
        @Override
        protected void onPostExecute(String retString) {
            super.onPostExecute(retString);
            Toast.makeText(MainActivity.this,retString,Toast.LENGTH_LONG).show();

            list.setAdapter(adapter);

            if(!isConnected()) {
                WriteJsonInLocalStorage();
            }


            Toast.makeText(getApplicationContext(),"Data has been updated",Toast.LENGTH_LONG).show();
        }


        public void WriteJsonInLocalStorage() {

            String filename="comment.json";
            String data= jsonStr;

            FileOutputStream fos;

            try {
                fos = openFileOutput(filename, Context.MODE_PRIVATE);
                fos.write(data.getBytes());
                fos.close();

                Toast.makeText(getApplicationContext(), filename + " Saved", Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                Log.d("Main_activity",e.getMessage());
            }

        }

        public Boolean ParseJson () {
            try {
                JSONArray jsonObj = new JSONArray(jsonStr);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject c = jsonObj.getJSONObject(i);
                    String id_category = c.getString("id_category");
                    String name_category = c.getString("name_category");
                    String description = c.getString("description");

                    commentList.add(new CommentBlog(id_category, name_category, description));
                }
        //error catching
            } catch (final JSONException e) {
                Log.e(TAG, "Impossible to download the json file.");
                Toast.makeText(getApplicationContext(),"Impossible to download the json.",Toast.LENGTH_LONG).show();
                return false;
            }

            return true;
        }


        //Detect if the mobile is connected to the internet
        private boolean isConnected () {
            ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            boolean isWifiConn = networkInfo.isConnected();
            networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            boolean isMobileConn = networkInfo.isConnected();

            Log.d(TAG, "Wifi connected: " + isWifiConn);
            Log.d(TAG, "Mobile connected: " + isMobileConn);

            if (isWifiConn || isMobileConn) {
                return true;
            }else{
                return false;
            }

        }

    }



}
