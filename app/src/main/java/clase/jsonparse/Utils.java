package clase.jsonparse;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class Utils {


    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    private static String  getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String doGet(String name_category ,String description) {
        try {
            String response="";

            URL url = new URL("http://10.0.2.2/final_project/comment/api/comment/sData.php");

            //OPEN CONNECTION
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            response = Utils.convertStreamToString(conn.getInputStream());

            //CLOSE CONNECTION
            conn.disconnect();

            return response;
        }
        catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    public static String doPost(String name_category ,String description) {
        try {
            String response = "";

            URL url = new URL("http://10.0.2.2/final_project/comment/api/comment/Create.php");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            HashMap<String, String> postDataParams = new HashMap<String, String>();
            postDataParams.put("name_category", name_category);
            postDataParams.put("description", description);

            OutputStream os = conn.getOutputStream();
            BufferedWriter sender = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            sender.write(getPostDataString(postDataParams));

            sender.flush();
            sender.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
            return  response;

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("UTILS", e.getMessage());
            return null;
        }

    }
}

